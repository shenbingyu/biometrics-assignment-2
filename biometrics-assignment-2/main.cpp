//
//  main.cpp
//  changingtxtfile
//
//  Created by bingyu shen on 10/16/15.
//  Copyright © 2015 bingyu shen. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sstream>
#include <cstring>

using namespace std;

vector<string> getwords(vector<string>& words,string line){
    string::const_iterator end=line.end();
    string::const_iterator current=line.begin();
    string::const_iterator next=find(current, end, '\t');
    while (next!=end) {
        words.push_back(string(current,next));
        current=next+1;
        next=find(current, end, '\t');
    }
    words.push_back(string(current,end));
    return words;
    
}

int main(int argc, const char * argv[]) {
    // insert code here...
    
    string picname;
    ifstream fin("/openbr/data/LFW/pairsDevTest.txt");
    ofstream out1("/Users/bingyushen/Desktop/biometrics/samepairs.txt");
    ofstream out2("/Users/bingyushen/Desktop/biometrics/differentpairs.txt");
    int k=0;

    while (getline(fin,picname)) {
        string picname1="/openbr/data/LFW/img/";
        string picname2="/openbr/data/LFW/img/";
        string temp;
        vector<string> words;
        vector<string>::iterator ite;
        getwords(words, picname);
        int i=0;
        for(ite=words.begin();ite!=words.end();++ite){
            if (i==0) {
                picname1+=*ite;
                picname1+="/";
                picname1+=*ite;
                temp=picname1;
            }
            if (i==1) {
                if (stoi(*ite)<=9) {
                    picname1+="_000";
                    picname1+=*ite;
                    picname1+=".jpg";
                    
                }
                else{
                    picname1+="_00";
                    picname1+=*ite;
                    picname1+=".jpg";
                }
            }
            if (i==2) {
                string nameornumber=*ite;
                if ((nameornumber[0]>='0')&&(nameornumber[0]<='9')) {
                    picname2=temp;
                    if (stoi(*ite)<=9) {
                        picname2+="_000";
                        picname2+=*ite;
                        picname2+=".jpg";
                    }
                    else if ((stoi(*ite)>9)&&(stoi(*ite)<100)){
                        picname2+="_00";
                        picname2+=*ite;
                        picname2+=".jpg";
                    }
                    else{
                        picname2+="_0";
                        picname2+=*ite;
                        picname2+=".jpg";
                    }
                }
                else{
                    picname2+=*ite;
                    picname2+="/";
                    picname2+=*ite;
                }
                
            }
            if (i==3) {
                if (stoi(*ite)<=9) {
                    picname2+="_000";
                    picname2+=*ite;
                    picname2+=".jpg";
                }
                else if ((stoi(*ite)>9)&&(stoi(*ite)<100)){
                    picname2+="_00";
                    picname2+=*ite;
                    picname2+=".jpg";
                }
                else{
                    picname2+="_0";
                    picname2+=*ite;
                    picname2+=".jpg";
                }

            }
            
            i++;
        }
        k++;
        
        if (k<=501) {
            out1<<picname1<<"\t"<<picname2<<endl;
        } else {
            out2<<picname1<<"\t"<<picname2<<endl;
        }
        
        cout<<picname2<<endl;
    }
    
    fin.close();
    std::cout << "Hello, World!\n";
    return 0;
}
