import matplotlib.pyplot as plt

FRR=[]
FAR=[]

for threshold_big in range(-50,300,1):
    with open("/Users/bingyushen/Desktop/biometrics/samepairsscore.txt") as f:
        threshold=float(threshold_big)/100

        TAnum=0
        FRnum=0

        print "THREASHOLD---",threshold
        for line in f:

            if float(line[:6])<=threshold:
                FRnum=FRnum+1

            else:
                TAnum=TAnum+1

    FRR.append(float(FRnum)/500)

for threshold_big in range(-50,300,1):
    with open("/Users/bingyushen/Desktop/biometrics/differentpairsscore.txt") as f:
        threshold=float(threshold_big)/100

        FAnum=0
        TRnum=0

        print "THREASHOLD---",threshold
        for line in f:

            if float(line[:6])>threshold:
                FAnum=FAnum+1

            else:
                TRnum=TRnum+1
                    
    FAR.append(float(FAnum)/500)

print len(FAR)
print len(FRR)
X=[]
Y=[]
for x_ori in range(0,300,1):
    x=float(x_ori)/800
    X.append(x)
    Y.append(x)
plt.plot(FRR,FAR,"b-",X,Y,"r-")
plt.ylabel('Faulse Accept Rate')
plt.xlabel('False Reject Rate')
plt.savefig("/Users/bingyushen/Desktop/biometrics/openbrdet.png")
plt.show()
